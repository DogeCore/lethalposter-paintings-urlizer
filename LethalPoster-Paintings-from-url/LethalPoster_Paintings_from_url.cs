using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using HarmonyLib;
using UnityEngine.Android;
using UnityEngine.Networking;

namespace LethalPoster_Paintings_from_url;

[BepInPlugin(MyPluginInfo.PLUGIN_GUID, MyPluginInfo.PLUGIN_NAME, MyPluginInfo.PLUGIN_VERSION)]
public class LethalPoster_Paintings_from_url : BaseUnityPlugin
{
    public static LethalPoster_Paintings_from_url Instance { get; private set; } = null!;
    internal new static ManualLogSource Logger { get; private set; } = null!;
    internal static Harmony? Harmony { get; set; }
    private ConfigEntry<string>? configFivePostersUrls;
    private ConfigEntry<string>? configTipPostersUrls;
    private ConfigEntry<string>? configPaintingUrls;
    private ConfigEntry<string>? configFivePostersFolder;
    private ConfigEntry<string>? configPaintingsFolder;
    private ConfigEntry<string>? configTipPostersFolder;

    private string? defaultFivePostersFolder;
    private string? defaultTipPostersFolder;    
    private string? defaultPaintingsFolder;

    private void Awake()
    {
        Logger = base.Logger;
        Instance = this;

        string modDir = Path.GetDirectoryName(Info.Location);
        defaultFivePostersFolder = modDir +"\\LethalPosters\\posters\\";
        defaultTipPostersFolder = modDir +"\\LethalPosters\\tips\\";
        defaultPaintingsFolder = modDir +"\\LethalPaintings\\paintings\\";

        configPaintingUrls = Config.Bind("General",
                                         "paintingurls",
                                         "",
                                         "list of Urls for paiting files to download - space separated (ex : https://watev/1.png https://watev2.png ...) - must be using femboytv/LethalPaintings templates and be .png"); // Description of the option to show in the config file

        configFivePostersUrls = Config.Bind("General", 
                                            "FivePostersUrls",
                                            "",
                                            "list of Urls for atlas textures with 5 posters files to download - space separated (ex : https://watev/1.png https://watev2.png ...) - must be using femboytv/LethalPosters templates and be .png");
            
        configTipPostersUrls = Config.Bind("General", 
                                            "TipPostersUrls",
                                            "",
                                            "list of Urls for the textures for the Tips for the job poster files to download - space separated (ex : https://watev/1.png https://watev2.png ...) - must be using femboytv/LethalPosters templates and be .png");

        configFivePostersFolder = Config.Bind("General",
                                          "FivePostersFolder",
                                          "",
                                          "Leave blank unless you need to overwrite ! 5 posters folder to download into - must be what femboytv/LethalPosters excepts. Default will be BepInEx.Paths.PluginPath+LethalPosters/posters/"); // Description of the option to show in the config file

        configTipPostersFolder = Config.Bind("General",
                                          "TipPostersFolder",
                                          "",
                                          "Leave blank unless you need to overwrite ! Tips for the job posters folder to download into - must be using femboytv/LethalPosters excepts.  Default will be BepInEx.Paths.PluginPath+LethalPaintings/paintings/"); // Description of the option to show in the config file

        configPaintingsFolder = Config.Bind("General", 
                                           "PaintingsFolder",
                                           "",
                                           "Leave blank unless you need to overwrite ! Paintings folder to download into  - must be using femboytv/LethalPaintings excepts. Default will be BepInEx.Paths.PluginPath+LethalPaintings/paintings/");

        // do 5 posters
        string dir1 = configFivePostersFolder.Value.ToString() == "" ? defaultFivePostersFolder : configFivePostersFolder.Value.ToString();
        createDirIfNotExists(dir1);
        string dir2 = configTipPostersFolder.Value.ToString() == "" ? defaultTipPostersFolder : configTipPostersFolder.Value.ToString();
        createDirIfNotExists(dir2);
        string dir3 = configPaintingsFolder.Value.ToString() == "" ? defaultPaintingsFolder : configPaintingsFolder.Value.ToString();
        createDirIfNotExists(dir3);
    
        if(configFivePostersUrls.Value != ""){
            DownloadStuff(configFivePostersUrls.Value.Split(" "),dir1);
        }
        else Logger.LogWarning($"{MyPluginInfo.PLUGIN_GUID}: no atlas textures with 5 posters files to download !");
    
         // do tips
        if(configTipPostersUrls.Value != ""){

            DownloadStuff(configTipPostersUrls.Value.Split(" "), dir2);
        }
        else Logger.LogWarning($"{MyPluginInfo.PLUGIN_GUID}: no Tips for the job posters files to download !");

        // do paintings
        if(configPaintingUrls.Value != ""){

            DownloadStuff(configPaintingUrls.Value.Split(" "),dir3 );
        }
        else Logger.LogWarning($"{MyPluginInfo.PLUGIN_GUID}: no paintings files to load !");

    }

    private void DownloadStuff(string[] filelist, string folder) {
    
        foreach ( string url in filelist)
        {
            // -- unity donwload chenanigans
            IEnumerator DownloadFile() {

                var uwr = new UnityWebRequest(url, UnityWebRequest.kHttpVerbGET);

                // hashing url to use as filename, and skipping if exists. Internet bandwith save hype !
                string filepath = folder+ url.GetHashCode().ToString() +".png";

                if (!File.Exists(filepath))
                {
                    uwr.downloadHandler = new DownloadHandlerFile(filepath);
                    yield return uwr.SendWebRequest();
                    
                    if (uwr.result != UnityWebRequest.Result.Success){ 
                        Logger.LogError($"{MyPluginInfo.PLUGIN_GUID}: " + uwr.error);
                    }
                    else {
                        //checking .png magic bytes, because there are some bad pepole on the internet.
                        if (isPng(filepath)) {
                            Logger.LogInfo($"{MyPluginInfo.PLUGIN_GUID}: {url} successfully downloaded and saved to {filepath}");
                        }
                        else{
                            File.Delete(filepath);
                            Logger.LogError($"{MyPluginInfo.PLUGIN_GUID}: {url} was not a .png file, removed.");
                            
                        }

                    }                
                }
                else Logger.LogInfo($"{MyPluginInfo.PLUGIN_GUID}: {url} already exists at {filepath}");

            }
            StartCoroutine(DownloadFile());
        }
    }

    // checks if png header is right here, its bandaid tier security but hey...
    public bool isPng(string filePath)
    {
        byte[] pngBytesRef = [0x89,0x50,0x4E,0x47,0x0D,0x0A,0x1A,0x0A];
        byte[] fileBytes = new byte[pngBytesRef.Length];
        using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
        {
            fs.Read(fileBytes, 0, pngBytesRef.Length);
        }
        return pngBytesRef.SequenceEqual(fileBytes);
    }

    public void createDirIfNotExists(string dir){
    if (!Directory.Exists(dir)) 
        {
            Logger.LogWarning($"{MyPluginInfo.PLUGIN_GUID}: creating dir : {dir}");
            Directory.CreateDirectory(dir);
        }
    }
}
