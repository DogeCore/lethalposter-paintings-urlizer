# LethalPoster-Paintings-from-url

## Description :

Short mod to allow downloading files for LethalPaintings & LethalPosters, rather than creating a mod to add them.  
If - like me - you don't fançy slapping your friend's butt for all to see on thunderstore...

Using it amounts to simply slaping an url pointing to a .png file (in the right templated format) in the config, it will download it at load. Will keep it there for next time, so you save bandwith and dont have to download heaps of pictures each time.  
Bear in mind it only checks if the donwloaded file is a .png, no other fancy protection besides your common sense - i dont have to remind you to not blindly trust someone elses link right?

Didn't find a mod already made to do so, so i did it, probably badly idk it's my first mod like ever so good luck, have fun.

It's very odd, especially concidering the amount of web boombox mods out there. But hey maybe it's well hidden or something idk.

## Installation :

Use R2modman.

or to do it yourself :

Get one of the [gitlab releases](https://gitlab.com/DogeCore/lethalposter-paintings-urlizer/-/releases), and dump the DogeCore_LethalPoster_Paintings_from_url folder into bepinex/plugins, and voilà.  
Required folders are created if absent. Config files will only exist after one first launch of the game, so prepare to restart it at least once.

Might wana also read [https://lethal.wiki/.](https://lethal.wiki/.)

## Usage :

All is in the config. Just slap your urls in the right variable and you'll be done.

For R2Modman users, look in the config editor for somthing along **LethalPoster-Paintings-from-url.cfg,** and edit away.

Or fetch and edit the file in your \*_BepInEx\\config\\\*_ folder.

In the config, those are the actual lists of images to dl :

- paintingurls : list of Urls for paiting files to download - space separated (ex : https://watev/1.png https://watev2.png ...) - must be using femboytv/LethalPaintings templates and be .png
- FivePostersUrls : list of Urls for atlas textures with 5 posters files to download - space separated (ex : https://watev/1.png https://watev2.png ...) - must be using femboytv/LethalPosters templates and be .png
- TipPostersUrls : list of Urls for the textures for the Tips for the job poster files to download - space separated (ex : https://watev/1.png https://watev2.png ...) - must be using femboytv/LethalPosters templates and be .png

Mod should accept any link that points to a image download, or an image link, as long as it's available without login. If not, lookout for the logs in the console, it will tell you what's wrong.

To note : it does not clean anything between changes, so if you want to remove stuff, you'll have to do it yourself by manually deleting the files in the folder.

- As an exemple, if you wanted to use google drive as a provider :
  - just generate a share link, get the file id (the random string after file/d/ in the link url) and put it after this url : https://drive.google.com/uc?export=download&id=  
    Should work wonders.

Also, additional config options. I have set those in the defaults, so nothing needed, but hey if you really need to overwrite them :

- FivePostersFolder : 5 posters folder to download into - must be what femboytv/LethalPosters excepts. Default will be Location+LethalPosters/posters/
- TipPostersFolder : Tips for the job posters folder to download into - must be using femboytv/LethalPosters excepts. Default will be Location+LethalPaintings/paintings/
- PaintingsFolder : Paintings folder to download into - must be using femboytv/LethalPaintings excepts. Default will be Location+LethalPaintings/paintings/

## Support / Toubleshooting :

- First: If it dosent work properly first time, and you don't see the posters / paintings, it might be LethalPaintings / LethalPosters loading before this mod. Both mods are not in the bepinex dependencies to alleviate this, + the naming should prevent this. But in doubt, try to restart your game. Files will be in place for the second run so it won't be an issue.
- Second: Mod should log everything of importance in the bepinex console, so look in it if you have problems, should solve the remaining 90% of issues.
  - Aka download problems, file not png... it spews the log of the UnityWebRequest made directly so should be pretty telling.
- Finaly, if you spot something else, open an issue, will see what i can do. Probably not much, but hey try anyway. Might jab at it if bored.

## Authors and acknowledgment :

Credits and thanks goes to Femboytv for LethalPaintings & LethalPosters : https://thunderstore.io/c/lethal-company/p/femboytv/ / https://femboy.tv   
For the rest: made by DogeCore - 2024

## License :

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.  
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.  
You should have received a copy of the GNU General Public License along with this program. If not, see \<https://www.gnu.org/licenses/\>.

Original author acknowledgement optional but appreciated.
